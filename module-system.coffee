
do ->
	
	global = @

	ensureModule = (name) ->
		pkg = global
		parts = name.split(/\./);
		for part in parts
			if not pkg[part]
				pkg[part] = {}
			
			pkg = pkg[part];

		return pkg

	global.Module = (name, func) ->
		pkg = ensureModule name

		pkg.INIT_FUNCS ?= []
		pkg.INIT_FUNCS.push func

		return pkg

	global.require = (name) ->
		module = ensureModule name
		if module.INITING
			throw new Error("Cyclic dependency on " + name)

		if module.INIT_FUNCS
			module.INITING = true
			for f in module.INIT_FUNCS
				f module

			module.INIT_FUNCS = null
			module.INITING = null

		return module

