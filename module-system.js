(function() {
  var ensureModule, global;
  global = this;
  ensureModule = function(name) {
    var part, parts, pkg, _i, _len;
    pkg = global;
    parts = name.split(/\./);
    for (_i = 0, _len = parts.length; _i < _len; _i++) {
      part = parts[_i];
      if (!pkg[part]) {
        pkg[part] = {};
      }
      pkg = pkg[part];
    }
    return pkg;
  };
  global.Module = function(name, func) {
    var pkg;
    pkg = ensureModule(name);
    if (pkg.INIT_FUNCS == null) {
      pkg.INIT_FUNCS = [];
    }
    pkg.INIT_FUNCS.push(func);
    return pkg;
  };
  global.require = function(name) {
    var f, module, _i, _len, _ref;
    module = ensureModule(name);
    if (module.INITING) {
      throw new Error("Cyclic dependency on " + name);
    }
    if (module.INIT_FUNCS) {
      module.INITING = true;
      _ref = module.INIT_FUNCS;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        f = _ref[_i];
        f(module);
      }
      module.INIT_FUNCS = null;
      module.INITING = null;
    }
    return module;
  };
  global.reqwire = global.require;
})();